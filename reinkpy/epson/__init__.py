# SPDX-License-Identifier: AGPL-3.0-or-later
"""Epson Printers"""

__all__ = (
    'EpsonPrinter',
    'MODELS',
)

from .core import EpsonPrinter
from .models import MODELS
